import { Container } from "./style";

const Projects = () => {
    return (
        <Container>
            <h2>Projetos pessoais</h2>
            <p>
                <a 
                    target="_blank"
                    rel="noreferrer"
                    href="https://gitlab.com/miguel-maurer/batalha-naval"
                >
                
               Batalha-naval</a> criado em C, usando alocações dinâmicas e matrizes. Trabalho 1 de Laboratório de Programação 2 da faculdade.
            </p>
            <p>
                <a 
                    target="_blank"
                    rel="noreferrer"
                    href="https://miguel-maurer.gitlab.io/assessment-instagram-social-network-platform-layout/"
                >
                
                Página de perfil do Instagram</a> usando HTML5 e CSS3. Para desenvolver minhas habilidades com HTML e pegar o jeito com CSS.
            </p>
            <p>
                <a 
                    target="_blank"
                    rel="noreferrer"
                    href="https://miguel-maurer.gitlab.io/game-of-chance/"
                >
                
                Minigames</a> usando JavaScript e DOM. Composto por três jogos: Jokenpo, Magic 8-ball e um Caça-níquel. Possui light theme e dark theme.
            </p>
            <p>
                <a 
                    target="_blank"
                    rel="noreferrer"
                    href="https://capstone-group1.vercel.app"
                >
                
                Projeto</a> <span>feito em grupo usando ReactJS e algumas bibliotecas como styled-components, react-router-dom, hook-form, axios, react-icons e Chakra-ui.</span>
                <span>O desafio foi criar uma aplicação que ajudasse com o controle de estoque desde de start-ups até grandes empresas.</span>
                <span>Trabalhamos com metodologias ágeis (SCRUM) e concluimos o projeto em duas semanas usando JSON-server para a API.</span>
            </p>
        </Container>
    )
}

export default Projects;