import Routes from "./routes"
import { ToastContainer } from 'react-toastify';

function App() {
  return (
    <>
      <Routes/>
      <ToastContainer
        position="top-center"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </>
  );
}

export default App;
