import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
    :root {
        --background: #27273d;
        --orange: #FF7900;
        --pink: #AB00AA;
        --blue: #0019FF;
        --white: #f5f5f5;
        --black: #0c0c0c;
        --font: 'Raleway', sans-serif;
    }


    html, body, div, span, applet, object, iframe,
    h1, h2, h3, h4, h5, h6, p, blockquote, pre,
    a, abbr, acronym, address, big, cite, code,
    del, dfn, em, img, ins, kbd, q, s, samp,
    small, strike, strong, sub, sup, tt, var,
    b, u, i, center,
    dl, dt, dd, ol, ul, li,
    fieldset, form, label, legend,
    table, caption, tbody, tfoot, thead, tr, th, td,
    article, aside, canvas, details, embed, 
    figure, figcaption, footer, header, hgroup, 
    menu, nav, output, ruby, section, summary,
    time, mark, audio, video, input, select {
        margin: 0;
        padding: 0;
        border: 0;
        outline: none;
    }

    body {
        background-color: var(--black);
        font-family: var(--font);
    }


`;

export default GlobalStyle;