import Border from "../border";
import { Container } from "./style";
import { MdEmail } from "react-icons/md";
import { IoLogoWhatsapp } from "react-icons/io";
import { BsLinkedin, BsInstagram } from "react-icons/bs";
import { CopyToClipboard } from 'react-copy-to-clipboard';
import toast from "react-hot-toast";

const Contact = () => {

    const notify = (texto) => toast.success(texto);

    return  (
        <>
            <Border footer={true}/>
            <Container>
                <p>
                    <MdEmail/>
                    <CopyToClipboard
                        text="miguel.maurer2001@gmail.com"
                    > 
                        <button onClick={() => notify('E-mail copiado com sucesso.')}>miguel.maurer2001@gmail.com</button>
                    </CopyToClipboard> 

                </p>
                <p>
                    <IoLogoWhatsapp/>
                    <CopyToClipboard
                        text="55996254242"
                    > 
                        <button onClick={() => notify('Número copiado com sucesso.')}>(55) 9 9625-4242</button>
                    </CopyToClipboard>
                </p>
                <p>
                    <BsLinkedin/> 
                    <span><a href="https://www.linkedin.com/in/miguel-maurer/" target="_blank" rel="noreferrer">Linkedin</a></span>
                </p>
                <p>
                    <BsInstagram/> 
                    <span><a href="https://www.instagram.com/miguelmaurer/" target="_blank" rel="noreferrer">Instagram</a></span>
                </p>
            </Container>
        </>
    )
}

export default Contact;