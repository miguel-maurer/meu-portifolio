import styled from "styled-components";

export const Container = styled.div`
    width: 100%;
    background-color: #222;
    color: var(--white);
    display: flex;
    justify-content: space-around;

    p {
        margin: 2rem 0;
        font-size: 1.2rem;
        a {
            color: var(--white);
            font-size: 1.2rem;
            padding: 10px;
        }
    }

    button {
        background-color: #222;
        border: none;
        color: var(--white);
        cursor: pointer;
        text-decoration: underline;
        font-size: 1.2rem;
    }
`;