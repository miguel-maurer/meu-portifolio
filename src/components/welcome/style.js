import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    width: 85%;
    height: 75vh;
    padding: 2rem;
    margin: 2rem;
    color: var(--white);
    
    h1 {
        margin: 2rem 0;
    }

    p {
        margin: 0.8rem;
        font-size: 1.2rem;
    }

    img {
        width: 200px;
        position: relative;
        border-radius: 50%;
        border: 2px solid var(--white);
        padding: 10px;
        bottom: 95%;
        left: 65%;
        box-shadow: 0px 0px 110px 40px #6054FF;
    }
`;

export const External = styled.div`
    display: flex;
    justify-content: center;
`;

export const Cards = styled.div`
    position: relative;
    width: 20%;
    padding: 1rem;
    border-radius: 10px;
`;

export const WhoAmI = styled(Cards)`
    background: linear-gradient(to right, #FF7900 0%, #AB00AA 100%, #6054FF 100%);
`;

export const Started = styled(Cards)`
    left: 10%;
    top: 5%;
    background: linear-gradient(to right, #FF7900 0%, #AB00A4 80%, #837EC4 100%);
`;

export const Facul = styled(Cards)`
    left: 20%;
    top: 10%;
    width: 35%;
    background: linear-gradient(45deg, #FF7900 0%, #AB00AA 64%, #6054FF 100%);
`;

export const FstContact = styled(Cards)`
    left: 60%;
    top: -10%;
    width: 40%;
    background: linear-gradient(to bottom, #6054FF 0%, #AB00AA 50%, #FF7900 100%);
`;

