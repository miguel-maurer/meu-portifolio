import Contact from "../components/contact";
import Projects from "../components/projects";
import Skills from "../components/skills";
import Welcome from "../components/welcome";
import { Container } from "./style.home";
import { Toaster } from 'react-hot-toast';

const Home = () => {
    return(
        <Container>
            <div><Toaster/></div>
            <Welcome/>
            <Skills/>
            <Projects/>
            <Contact/>
        </Container>
    )
}

export default Home;