import styled from "styled-components";

export const B = styled.div`
    background: linear-gradient(to right, #FF7900 0%, #AB00AA 50%, #0019FF 100%);
    height: 2px;
    width: ${props => props.footer ? "100%" : "85%"};
`;