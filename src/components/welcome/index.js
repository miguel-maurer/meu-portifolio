import Border from "../border";
import { Container, External, Facul, FstContact, Started, WhoAmI } from "./style";

const Welcome = () => {

    return (
        <>
            <External>
            <Container>
                <h1>Seja bem-vindo(a) ao meu protifólio.</h1>
                <WhoAmI>
                    <h2>Quem sou eu?</h2>
                    <p>Meu nome é Miguel, tenho 20 anos, nascido em Santiago e moro atualmente em Santa Maria</p>
                </WhoAmI>
                <Started>
                    <h2>Aonde tudo começou</h2>
                    <p>Desde pequeno tive muita afinidade com os computadores, meu pai é programador e me incluiu nesse mundo também.</p>
                </Started>
                <Facul>
                    <h2>Faculdade</h2>
                    <p>Em 2019 quando decidi que curso gostaria de fazer na faculdade, optei pela Engenharia da computação, pois era apaixonado por hardware. No fim acabei passando pra Sistemas de Informação e nesse curso descobri a programação e finalemente me achei.</p>
                </Facul>
                <FstContact>
                    <h2>A programação</h2>
                    <p>Comecei a aprender C e quis buscar mais. Achei o curso de full-stack da Kenzie, me interessei e resolvi cursar também. Aprendi novas maneiras de programar, outras linguagens e desenvolvi soft-skills. Além de tudo, ainda trabalho como Coach, ajudando pessoas que assim como eu buscam um novo caminho na programação.</p>
                </FstContact>
                <img src="https://media-exp1.licdn.com/dms/image/C5603AQGdtIdizqy3fg/profile-displayphoto-shrink_200_200/0/1623871206577?e=1642032000&v=beta&t=JSD1HpAi7q4dgms5t2p60G0r4ejyiBWC571urBMu-wo" alt="teste"/>
            </Container>
            </External>
            <Border></Border>
        </> 
        )
}

export default Welcome;