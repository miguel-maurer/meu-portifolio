import styled from "styled-components";

export const Container = styled.div`
    color: var(--white);
    padding: 3rem;
    display: flex;
    flex-direction: column;
    width: 85%;

    figure {
        display: flex;
        align-items: center;
        justify-content: space-between;

        figcaption {
            max-width: 60%;
            font-size: 1.2rem;
        }
    }
`;

export const Imgs = styled.img`
    margin: 40px 0;
    ${props => props.little ? 'width: 200px' : 'width: 100px'};
    ${props => props.space ? 'margin-left: 0' : 'margin-left: 40px'};
`;