import Border from "../border";
import { Container, Imgs } from "./style";

const Skills = () => {
    return (
        <>
            <Container>
                <figure>
                    <Imgs src="https://i.imgur.com/DfRUdHT.png"/>
                    <figcaption>Com C fiz meu primeiro "Hello World!" e meu primeiro jogo, com a ajuda da biblioteca gráfica Allegro.</figcaption>
                </figure>
                <figure>
                    <Imgs little src="https://i.imgur.com/71YAvew.png"/>
                    <figcaption>Aqui descobri o mundo do desenvolvimento web, foi um desafio aprender CSS, mas com prática peguei o jeito.</figcaption>
                </figure>
                <figure>
                    <Imgs src="https://i.imgur.com/5F2xWoP.png"/>
                    <figcaption>Com JS consegui incrementar meus projetos de HTML e ir ainda mais longe, sendo capaz de fazer jogos e sites funcionais.</figcaption>
                </figure>
                <figure>
                    <Imgs space little src="https://i.imgur.com/wMgEOew.png"/>
                    <figcaption>Com React me senti completo, aumentou a complexidade, mas da mesma forma facilitou muito. Criei formulários, rotas, aprendi redux, providers. Uma forma muito mais prática de codar com JS.</figcaption>
                </figure>
                <figure>
                    <Imgs src="https://i.imgur.com/Mt6N9b0.png"/>
                    <figcaption>Por fim python está me introduzindo no mundo do Back-end, onde estou ganhando uma nova visão das coisas e aprendendo cada vez mais.</figcaption>
                </figure>
            </Container>
            <Border></Border>
        </>
        )
}

export default Skills;