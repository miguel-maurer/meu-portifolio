import styled from "styled-components";

export const Container = styled.div`
    color: white;
    display: flex;
    flex-direction: column;
    width: 85%;
    padding: 2rem;

    h2 {
        font-size: 2rem;
    }
    p {
        padding: 3rem;
        font-size: 1.3rem;

        a {
            color: var(--white);
            font-weight: 600;
            
            &:visited{
                color: var(--white);
            }
        }

        span {
            margin-top: 2rem;
        }
    }
`;