import { B } from './style';

const Border = ({ footer }) => {
    return <B footer={footer}></B>
}

export default Border;